# Examen Indigo

This is a examen for indigo by johernandez

## Technology 
* Stack MEAN
    * [Mongo](https://www.mongodb.com/)
    * [ExpressJs](http://expressjs.com/es/)
    * [Angular](https://angular.io/)
    * [NodeJS](https://nodejs.org/es/)

### Prerequisites
This project needs
- Mongodb running through the default port
- Angular-cli installed in the machine
- NodeJS installed in the machine additional to this you need the installed[nodemon](https://nodemon.io/) package


### Installing

You need to clone the project

```
git clone https://DignityMasterpiece@bitbucket.org/DignityMasterpiece/examen.git
```

Enter the created folder

```
cd examen/
```

If it's the first time you run the project 

```
npm run first-deployment
```
If it is not the first time you run the project 

```
npm run deployment
```

The Application must be published [Here](http://localhost:3000/)
