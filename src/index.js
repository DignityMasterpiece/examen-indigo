const express = require('express');
const cors = require('cors');
const app = express();
const path = require('path');
//routes
const apiRoute = require('./routes/api');

//settings
app.set('port',process.env.PORT || 3000);

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:false}));

//routes
app.use('/api',apiRoute);

// static files 
app.use(express.static(path.join(__dirname,'angularClient/dist/angularClient')));
app.get('*', function(req, res) {
    console.log('?')
    res.sendFile(path.join(__dirname,'angularClient/dist/angularClient/index.html'));
});

//start
app.listen(app.get('port'), () => {
    console.log('server on ',app.get('port'));
})