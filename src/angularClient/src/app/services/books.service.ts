import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from "rxjs/operators";
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { Book } from '../Book';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  domain: string = "http://localhost:3000";
  constructor(private http: HttpClient) { }

  /**
   * Get All Books
   */
  getBooks() {
    return this.http.get<Book[]>(`${this.domain}/api/books`)
      .pipe(map((response: any) => response));
  };

  /**
   * Get specific Book
   */
  getBook(id) {
    return this.http.get(`${this.domain}/api/book/${id}`)
      .pipe(map((response: any) => response));
  };

  /**
   * Get specific Book
   */
  addBooks(newBook: Book) {
    return this.http.post(`${this.domain}/api/books`, newBook)
      .pipe(map((response: any) => response));
  };

  /**
   * Delete specific Book
   */
  deleteBooks(deletedBook: Book) {
    return this.http.delete(`${this.domain}/api/book/${deletedBook._id}`)
      .pipe(map((response: any) => response));
  };

  /**
   * Update specific Book
   */
  updateBook(updatedBook: Book) {
    return this.http.put(`${this.domain}/api/book/${updatedBook._id}`, updatedBook)
      .pipe(map((response: any) => response),
        catchError(this.handleError('updateBook', [])));
  };
  
  /**
   * Helper Handler error 
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return throwError(error.message || 'Server Error');
    };
  }
}
