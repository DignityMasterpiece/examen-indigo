import { Component, OnInit } from '@angular/core';
import { Book } from '../../Book';
import { BooksService } from '../../services/books.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ELEMENT_PROBE_PROVIDERS } from '@angular/platform-browser/src/dom/debug/ng_probe';
import { reject } from 'q';
import { DEFAULT_INTERPOLATION_CONFIG } from '@angular/compiler';


@Component({
  selector: 'app-new-book',
  templateUrl: './new-book.component.html',
  styleUrls: ['./new-book.component.css']
})
export class NewBookComponent implements OnInit {
  _id: string;
  errorMsg: string;
  book: Book = <Book>{};
  constructor(private booksService: BooksService, private route: ActivatedRoute, private router: Router) { }
  
  /**
   * Verify the parameter id by the route 
   * if exist get specific document in the book
   * collection to edit.
   */
  ngOnInit() {
    this._id = this.route.snapshot.paramMap.get('id');
    if (this._id) {
      this.booksService.getBook(this._id)
        .subscribe(book => {
          console.log(book);
          this.book = book;
        });
    }
  }

  /**
   * Verify the id book object 
   * if exist update the book document and send to view list
   * else add new book in books collection
   */
  saveChanges() {
    if (this.book._id) {
      this.isComplete().then(flag => {
        if (flag) {
          this.booksService.updateBook(this.book)
            .subscribe(result => {
              if (result.nModified === 1) {
                alert('Actualizado exitosamente!');
                this.router.navigate(['/list']);
              } else {
                alert('Algo salió mal!');
              };
            },
              error => {
                this.errorMsg = error;
              }
            );
        } else {
          this.errorMsg = 'Complete Campos Correctamente';
        }
      })
    } else {
      this.isComplete().then(flag => {
        if (flag) {
          this.booksService.addBooks(this.book)
            .subscribe(result => {
              if (result._id) {
                alert('Actualizado exitosamente!');
                this.router.navigate(['/list']);
              } else {
                alert('Algo salió mal!');
              };
            },
              error => {
                this.errorMsg = error;
              }
            );
        } else {
          this.errorMsg = 'Complete Campos Correctamente';
        }
      })
    }
  }

  /**
   * Validate if book object is complete.
   * @returns {Promise} Sync code.
   */
  isComplete() {
    return new Promise(resolve => {
      let elements: string[] = ['title', 'writer', 'pages', 'editorial'];
      elements.map(element => {
        if (!this.book[element]) {
          resolve(false);
        }
      });
      resolve(true);
    });
  }

}
