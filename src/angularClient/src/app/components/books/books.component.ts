import { Component, OnInit } from '@angular/core';
import { BooksService } from '../../services/books.service';
import { Book } from '../../Book';
import { Router } from '@angular/router';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books: Book[];

  constructor(private booksService: BooksService, private router: Router) {
  }

  /**
   * Get all the documents in the book collection.
   */
  ngOnInit() {
    this.getAll();
  }

  /**
   * Render view book edit.
   * @param {Object} book Book document to edit.
   */
  onSelect(book) {
    this.router.navigate(['/book', book._id]);
  }

  /**
   * Delete a book collection
   * @param book 
   */
  deleteBook(book) {
    if (confirm(`Desea eliminar el libro: ${book.title}`)) {
      this.booksService.deleteBooks(book)
      .subscribe(result => {
        console.log(result)
        if(result.deletedCount === 1) {
          this.getAll();
        } else {
          alert('Algo salió mal!');
        }
      });
    }
  }

  /**
   * Get all the documents in the book collection.
   */
  getAll(){
    this.booksService.getBooks()
    .subscribe(books => {
      this.books = books;
    });
  }

}
