export class Book {
    _id?: string;
    title: any;
    writer: string;
    pages: number;
    editorial: string;
}