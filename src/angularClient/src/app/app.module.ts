import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BooksComponent } from './components/books/books.component';
import { NewBookComponent } from './components/new-book/new-book.component';
import { BooksService } from './services/books.service';
const appRoutes: Routes = [
  { path: 'book/:id', component: NewBookComponent },
  {
    path: 'list',
    component: BooksComponent,
    data: { title: 'page 1' }
  },
  {
    path: 'new',
    component: NewBookComponent,
    data: { title: 'pepe' }
  },
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NewBookComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    ),
    AngularFontAwesomeModule
  ],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
export const routingComponents = [
  BooksComponent,
  NewBookComponent
]
