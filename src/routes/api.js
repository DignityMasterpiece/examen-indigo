const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('examen', ['books']);

/**
 * Get all the documents in the book collection.
 * @returns {Object[]} Array of objects.
 */
router.get('/books', (req, res, next) => {
    db.books.find((err, books) => {
        if (err) return next(err);
        res.json(books);
    });
});

/**
 * Get specific document in the book collection.
 * @param {String} req.params.id _id element on book document.
 * @returns {Object} Book document.
 */
router.get('/book/:id', (req, res, next) => {
    db.books.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, book) => {
        if (err){
            return next(err);
        } else {
            res.json(book);
        }
    });
});

/**
 * Insert one document in book collection.
 * @param {Object} req.body document book.
 * @returns Inserted document.
 */
router.post('/books', (req, res, next) => {
    let book = req.body;
    if (!book.title) {
        res.status(400).json({ err: 'no es posible agregar' });
    } else {
        db.books.save(book, (err, books) => {
            if (err) return next(err);
            res.json(books);
        });
    }
});

/**
 * delet one document in book collection.
 * @param {String} req.params.id id of document book.
 * @returns {Object} Operation result.
 */
router.delete('/book/:id', (req, res, next) => {
    db.books.remove({ _id: mongojs.ObjectId(req.params.id) }, (err, result) => {
        if (err) return next(err);
        res.json(result);
    });
});

/**
 * Update one document in book collection.
 * @param {Object} req.body document book.
 * @returns {Object} Operation result.
 */
router.put('/book/:id', (req, res, next) => {
    let book = req.body;
    delete book._id;
    db.books.update({ _id: mongojs.ObjectId(req.params.id)}, book, (err, book) => {
        if (err) {
            res.status(400).json({ err: 'no es posible actualizar' });;
        } else{
            res.json(book);
        }
    });
});

module.exports = router;